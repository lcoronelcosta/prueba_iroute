Para la realizacion de la instalacion y puesta en marcha de la aplicacion considerar los sig pasos

1. Primero instalar Symfony CLI
    url: https://symfony.com/download en nuestro caso el instalador de Windows
    Seguir los pasos correspodiente por lo general Next, Next
2. Instalar XAMP Server
    https://www.apachefriends.org/es/download.html
    Seguir los pasos correspodiente por lo general Next, Next
3. Instalar un SGBD en nuestro caso elegimos Heidy SQL por ser mas liviano
    https://www.heidisql.com/download.php
    Seguir los pasos correspodiente por lo general Next, Next
4. Correr el servicio de Xamp desde el acceso directo o la aplicacion de Xamp Control
5. Dentro de Xamp control correr el servivio de Mysql
6. Clonar el proyecto desde el repositorio
    Tomar el script de creacion de la BD y sus tablas que se encuentran en el repositorio
7. Abrir heidisql
8. Exportar la BD
9. Dentro del proyecto descargado previamente
    Correr el siguiente comando para instalar las dependencias del proyecto
    composer install
    Referencia: https://symfony.com/doc/current/setup.html
10. Para iniciar la aplicacion tiene que haber corrido e instalado la base de datos, para luego correr el comando
    symfony server:start
11. Abrir en el navegador la siguiente url para el login
    http://localhost:8000/login
    user por default: email:lufecoro@outlook.com | password: admin1234


